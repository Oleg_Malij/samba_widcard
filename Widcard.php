<?php
/**
 * Seotoaster 2.0 plugin bootstrap.
 *
 * @todo Add more comments
 * @author Seotoaster core team <core.team@seotoaster.com>
 */

class Widcard extends Tools_Plugins_Seosamba { //Tools_Plugins_Abstract

	/**
	 * List of action that should be allowed to specific roles
	 *
	 * By default all of actions of your plugin are available to the guest user
	 * @var array
	 */
	protected $_securedActions = array(
		Tools_Security_Acl::ROLE_SUPERADMIN => array(
            'secured'
        )
	);

	/**
	 * Init method.
	 *
	 * Use this method to init your plugin's data and variables
	 * Use this method to init specific helpers, view, etc...
	 */
	private $_roleId = null;

	public function  __construct($options, $seotoasterData) {
		parent::__construct($options, $seotoasterData);
		if ( $this->_user !== null ) {
				$this->_roleId = $this->_user->getRoleId();//$sessionHelper->getCurrentUser()->getRoleId();
		}
		$this->_currentUser = $this->_user;//$sessionHelper->getCurrentUser();
		$this->_view->setScriptPath(__DIR__ . '/system/views/');
	}

	/*protected function _init() {
		$this->_view->setScriptPath(__DIR__ . '/system/views/');
	}*/

	/**
	 * Main entry point
	 *
	 * @param array $requestedParams
	 * @return mixed $dispatcherResult
	 */
	public function run($requestedParams = array()) {
		$dispatcherResult = parent::run($requestedParams);
		//return ($dispatcherResult) ? method_exists($this, $dispatcherResult) : '';
		return ($dispatcherResult) ? $dispatcherResult : '';
	}

	protected function _makeOptionCard() {
		if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PAGE_PROTECTED)) {
		$sessionHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('Session');
			if($this->_request->isGet()) {
				$websiteInfo = WebsiteMapper::getInstance()->find($this->_sessionHelper->toasterId); //$this->_requestedParams['siteId']
				$this->_view->websiteUrlAction = $this->_websiteUrl;
				$this->_view->websiteUrl = $websiteInfo->getUrl();
				$this->_view->sambaUrl = $this->_websiteUrl;
				$wicData = unserialize($websiteInfo->getWicData());
				$this->_view->countryList = Zend_Locale::getTranslationList('territory', $sessionHelper->getCurrentUser()->getLanguage(), 2);
				if(isset($wicData['wicOrganizationCountry'])) {
					if($wicData['wicOrganizationCountry'] == 'US') {
						$this->_view->stateList = Tools_Samba_Tools::$stateListUSA;
					}
					elseif($wicData['wicOrganizationCountry'] == 'CA') {
						$this->_view->stateList = Tools_Samba_Tools::$stateListCanada;
					}
					else {$this->_view->stateList = '';}
				}

				$confFilePath = Zend_Registry::get('website');
				$confFilePath = $confFilePath['path'].'plugins/seofeatures/config/paymentType.ini';
				$paymentTypes = new Zend_Config_Ini($confFilePath);

				$this->_view->organizationName = $wicData['wicOrganizationName'];
				$this->_view->corporateLogo = ( ($wicData['wicCorporateLogo'] != '') && (is_array($wicData['wicCorporateLogo'])) ) ? 'data:image/'.$wicData['wicCorporateLogo']['extension'].';base64,'.$wicData['wicCorporateLogo']['src'] : $this->_websiteUrl.'system/images/noimage.png';
				$this->_view->organizationDescription = $wicData['wicOrganizationDescription'];
				$this->_view->organizationCountry = $wicData['wicOrganizationCountry'];
				$this->_view->address1 = $wicData['wicAddress1'];
				$this->_view->address2 = $wicData['wicAddress2'];
				$this->_view->city = $wicData['wicCity'];
				$this->_view->countryState = $wicData['wicCountryState'];
				$this->_view->zip = $wicData['wicZip'];
				$this->_view->phone = $wicData['wicPhone'];
				$this->_view->email = $wicData['wicEmail'];
				$this->_view->industryType = ($wicData['wicIndustryType'] != '') ? $wicData['wicIndustryType'] : '';
				$this->_view->business = ($wicData['wicBusiness'] != '') ? $wicData['wicBusiness'] : '';
				//$this->_view->gaCode = isset($wicData['wicGoogleAnalyticsCode']) ? $wicData['wicGoogleAnalyticsCode'] : '';
				//$this->_view->webAnalyticsCode = isset($wicData['wicWebAnalyticsCode']) ? $wicData['wicWebAnalyticsCode'] : '';
				$this->_view->paymentTypes = $paymentTypes->toArray();
				$this->_view->paymentType = isset($wicData['wicPaymentType']) ? $wicData['wicPaymentType'] : '';
				$this->_view->payOnline = isset($wicData['wicPayOnline']) ? $wicData['wicPayOnline'] : '';
				if(isset($wicData['language'])) {
					if( strlen($wicData['language']) <= 2 ) {
						$languageHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('language');
						$langList = $languageHelper->getLanguages(false);
						$this->_view->websiteLang = $langList[$wicData['language']];
					}
					else {$this->_view->websiteLang = $wicData['language'];}
				}
				else {
					$this->_view->websiteLang = '';
				}

				//$this->_view->webSiteId = $this->_requestedParams['siteId'];
				$apiPlugin = Tools_Factory_PluginFactory::createPlugin('api',array(), array('websiteUrl' => $this->_websiteUrl));
				$this->_view->industryList = $apiPlugin->getIndustryList();
				$this->_view->sambaUrl = $this->_websiteUrl;
				//echo json_encode(array('data' => $this->_view->render('siteinfo.phtml')));
				return $this->_view->render('widcard.phtml');
			}
		}
	}

	public function getStatesAction() {
		if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PAGE_PROTECTED)) {
			if($this->_requestedParams['countryCode'] == 'CA') {
				echo json_encode(array('states' => Tools_Samba_Tools::$stateListCanada));
			}
			elseif($this->_requestedParams['countryCode'] == 'US') {
				echo json_encode(array('states' => Tools_Samba_Tools::$stateListUSA));
			}
		}
	}

	public function saveSiteInfoAction() {
		if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PAGE_PROTECTED)) {
		$sessionHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('Session');
		//if(($sessionHelper->getCurrentUser()->getId()) != null) {
			$data = $this->_requestedParams;
			//$sessionHelper = Zend_Controller_Action_HelperBroker::getStaticHelper('Session');
			$website = WebsiteMapper::getInstance()->find($this->_sessionHelper->toasterId); //$data['webSiteId']
			$wicData = unserialize($website->getWicData());
			if( isset($wicData['wicCorporateLogo']) && ($wicData['wicCorporateLogo'] != '') && (is_array($wicData['wicCorporateLogo'])) ) {
				$data['imgExt'] = $wicData['wicCorporateLogo']['extension'];
				$data['imageSrc'] = $wicData['wicCorporateLogo']['src'];
			}
			$toaster = $this->_requestedParams['website'];
			$data['sambaToken'] = $this->_user->getToken();
			$synchronize = Tools_Factory_PluginFactory::createPlugin('api',array(), array('websiteUrl' => $this->_websiteUrl));
			$synchronizeResult = (object)$synchronize::request('put', 'websiteIdCard', $data, $this->_requestedParams['website'].'plugin/api/run/');

			if($synchronizeResult->done) {
				$website = WebsiteMapper::getInstance()->find($this->_sessionHelper->toasterId); //$data['webSiteId']

				$wicData = array(
					'wicOrganizationName' => $data['organization_name'],
					'wicOrganizationDescription' => $data['organization_description'],
					'wicOrganizationCountry' => $data['organization_country'],
					'wicAddress1' => $data['address1'],
					'wicAddress2' => $data['address2'],
					'wicCity' => $data['city'],
					'wicCountryState' => $data['country_state'],
					'wicZip' => $data['zip'],
					'wicPhone' => $data['phone'],
					'wicEmail' => $data['email'],
					'wicBusiness' => isset($data['business']) ? $data['business'] : '',
					'wicIndustryType' => isset($data['industry_type']) ? $data['industry_type'] : '',
					//'wicIsEcommerce' => isset($data['is_ecommerce']) ? '1' : '0',
					'wicPaymentType' => isset($data['payway']) ? $data['payway'] : '',
					'wicPayOnline' => isset($data['pay-online']) ? $data['pay-online'] : '',
					//'wicGoogleAnalyticsCode' => isset($data['gaCode']) ? $data['gaCode'] : '',
					'wicWebAnalyticsCode' => isset($data['webAnalyticsCode']) ? $data['webAnalyticsCode'] : '',
					'language' => isset($data['language']) ? $data['language'] : '',
					'wicCorporateLogo' => isset($data['imageSrc']) ? array('extension' => $wicData['wicCorporateLogo']['extension'], 'src' => $wicData['wicCorporateLogo']['src']) : $this->_websiteUrl.'system/images/noimage.png'
				);

				/*if(isset($data['imageSrc'])) {
					$wicData['wicCorporateLogo'] = array('extension' => $wicData['wicCorporateLogo']['extension'], 'src' => $wicData['wicCorporateLogo']['src']);
				}
				else {
					$wicData['wicCorporateLogo'] = $this->_websiteUrl.'system/images/noimage.png';//( (isset($wic['wicCorporateLogo'])) && ($wic['wicCorporateLogo'] != '') ) ? $wic['wicCorporateLogo'] : $this->_websiteUrl.'system/images/noimage.png';
				}*/
				$website->setWicData(serialize($wicData));
				$resSave = WebsiteMapper::getInstance()->save($website);
				$this->_redirector->gotoUrl($this->_websiteUrl.'site-dash.html');
			}
			else {
				$this->_redirector->gotoUrl($this->_websiteUrl.'site-dash.html');
			}
		}
	}

	public function uploadLogoAction() {
		if(Tools_Security_Acl::isAllowed(Tools_Security_Acl::RESOURCE_PAGE_PROTECTED)) {
			$uploadWebsiteIdCardImage = new Zend_File_Transfer_Adapter_Http();
			$fileInfo = $uploadWebsiteIdCardImage->getFileInfo();
			if($fileInfo['file']['type'] != null) {
				$miscConfig       = Zend_Registry::get('misc');
				$uploadWebsiteIdCardImage
				->addValidator('Extension', false,  array('jpg', 'jpeg', 'png', 'gif'))
				->addValidator(new Validators_MimeType(array('image/gif','image/jpeg','image/jpg','image/png')), false)
				->addValidator('ImageSize', false, array('maxwidth' => $miscConfig['imgMaxWidth'], 'maxheight' => $miscConfig['imgMaxWidth']));
				if($uploadWebsiteIdCardImage->isValid()) {
					$fileExtension = pathinfo($fileInfo['file']['name'], PATHINFO_EXTENSION);
					$imgBinary = fread(fopen($fileInfo['file']['tmp_name'], "r"), filesize($fileInfo['file']['tmp_name']));
					$src = base64_encode($imgBinary);
					$website = WebsiteMapper::getInstance()->find($this->_sessionHelper->toasterId);
					$wicData = unserialize($website->getWicData());
					$wicData['wicCorporateLogo'] = array('extension' => $fileExtension, 'src' => $src);
					$website->setWicData(serialize($wicData));
					$resSave = WebsiteMapper::getInstance()->save($website);
					$this->_responseHelper->success(array('src' => 'data:image/'.$fileExtension.';base64,'.$src));
				}
				else {
					$errMessage['imageUploadErr'] = $uploadWebsiteIdCardImage->getMessages();
					$this->_responseHelper->fail(array('data' => $errMessage['imageUploadErr']));
				}
			}
		}
	}

	public function saveToasterWidcard($params, $uid) {
		$wicData = array(
			'wicOrganizationName' => $params['wicOrganizationName'],
			'wicOrganizationDescription' => $params['wicOrganizationDescription'],
			'wicOrganizationCountry' => $params['wicOrganizationCountry'],
			'wicAddress1' => $params['wicAddress1'],
			'wicAddress2' => $params['wicAddress2'],
			'wicCity' => $params['wicCity'],
			'wicCountryState' => $params['wicCountryState'],
			'wicZip' => $params['wicZip'],
			'wicPhone' => $params['wicPhone'],
			'wicEmail' => $params['wicEmail'],
			'wicIndustryType' => ($params['wicIndustryType'] != '') ? unserialize($params['wicIndustryType']) : '',
			'language' => $params['language'],
			'wicBusiness' => $params['wicBusiness'],
			'wicPaymentType' => ($params['wicPaymentType'] != '') ? unserialize($params['wicPaymentType']) : '',
			'wicPayOnline' => isset($params['wicPayOnline']) ? $params['wicPayOnline'] : '',
			//'wicGoogleAnalyticsCode' => isset($params['wicGoogleAnalyticsCode']) ? $params['wicGoogleAnalyticsCode'] : ''
		);
		if(is_array($params['wicCorporateLogo'])) {
			$wicData['wicCorporateLogo'] = array('extension' => $params['wicCorporateLogo']['extension'], 'src' => $params['wicCorporateLogo']['src']);
		}
		else {
			$wicData['wicCorporateLogo'] = $this->_websiteUrl.'system/images/noimage.png';
		}
		$website = WebsiteMapper::getInstance()->getWebsiteByUrl($params['websiteUrl']);
		if($website != null) {
			$website->setWicData(serialize($wicData));
		}
		WebsiteMapper::getInstance()->save($website);
	}

	/**
	 * System hook to allow your plugin do some stuff before toaster controller starts
	 *
	 */
	public function beforeController() {

	}

	/**
	 * System hook to allow your plugin do some stuff after toaster controller finish its work
	 *
	 */
	public function afterController() {

	}

	/**
	 * Secured action.
	 *
	 * Will be available to the superadmin only
	 */
	public function securedAction() {

	}
}
