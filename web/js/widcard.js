$(document).ready(function(){
    if( $('select[name="industry_type[]"]').length ) {
		var selectedIndusrtries = $('select[name="industry_type[]"] option:selected')
        if(selectedIndusrtries.length != 0) {
			$.each(selectedIndusrtries, function(k, v){
				var liElement = $('<li>').attr('value', $(v).attr('value')).text($(v).attr('label'));
					$('#sel_industry').append(liElement);
                    $(liElement).append($('<span>').addClass('ui-icon ui-icon-closethick del-selected-industry').text('Delete'));
                    $('select[name="industry_type[]"] option[value='+$(v).attr('value')+']').addClass('selected-industry');
                });
                $('select[name="industry_type[]"] option:selected').removeAttr('selected');
         }
	}

	$('select[name="organization_country"]').live('change', function() {
        var stateField = $('select[name="country_state"]');
        if ($(this).val() == 'US' || $(this).val() == 'CA') {
            stateField.removeAttr('disabled')
            stateField.empty();
            showSpinner();
            $.getJSON($('#website_url').text()+'plugin/widcard/run/getStates', {'countryCode': $(this).val()}, function(response){
                $.each(response.states, function(code, name) {
                    stateField.append($('<option>').val(code).text(name));
                });
                hideSpinner();
            });
        }
        else{
            stateField.empty();
            stateField.attr('disabled',true);
        }
    });

    $('select[name="industry_type[]"]').live('change', function() {
        if($('select[name="industry_type[]"] option:selected').length >= 5) {
            $('select[name="industry_type[]"] option:selected:gt(4)').removeAttr("selected");
            $('select[name="industry_type[]"] option:not(:selected)').attr('disabled','disabled');
        }
        else {
            $('select[name="industry_type[]"] option:not(:selected)').removeAttr('disabled');
        }
    });



    $('select[name="industry_type[]"] option').live('click', function() {
        if( ($('#sel_industry li').length < 5) && ($(this).hasClass('selected-industry') != true) ) {
            $(this).addClass('selected-industry');
            var liElement = $('<li>').attr('value', $(this).val()).text($(this).text());
            $('#sel_industry').append(liElement);
            $(liElement).append($('<span>').addClass('ui-icon ui-icon-closethick del-selected-industry').text('Delete'));
        }
    });

    $('.del-selected-industry').live('click', function() {
        $('select[name="industry_type[]"] option[value='+$(this).parent().attr('value')+']').removeAttr('selected').removeClass('selected-industry'); //removeAttr('disabled').
        $(this).parent().remove();
    });

	$('div.payway-box').live('click', function(){
        var $checkbox = $('input:checkbox',this);
        $checkbox.attr('checked', !$checkbox.attr('checked'));
    });

	$('#saveWebsite').live('click', function(e) {
        e.preventDefault();
        $('select[name="industry_type[]"] option').removeAttr('selected');
        var industries = $('#sel_industry li');
        if(industries.length) {
                $.each(industries, function() {
                    $('select[name="industry_type[]"] option[value='+$(this).attr('value')+']').attr('selected','selected');
                });
        }

        var description = $('#teaser_text');
        if($(description).val().length > 200) {
            var posXY = $(description).position();
            $(description).addClass('warning');
            $('html,body').animate({
                scrollTop: posXY.top + 'px'
            }, 100);
            return false;
        }
        else {
            $(description).removeClass('warning');
        }

        $('#idCard').submit();
    });

    $(function() {
        $('#page-teaser-uploader-pickfiles').button();
        if(typeof(plupload) != "undefined") {
        var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,html4',
                browse_button : 'page-teaser-uploader-pickfiles',
                container : 'page-teaser-uploader-filelist',
                max_file_size : '10mb',
                max_file_count: 10,
                //resize : {width : <?php echo $maxWidth; ?>, height : <?php echo $maxHeight; ?>, quality : <?php echo $this->config['imgQuality']; ?>},
                url : '/plugin/widcard/run/uploadLogo',
                filters : [{title : "image",extensions : "png,jpg,jpeg,gif"}]
        });
        uploader.init();

        uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                        $('#page-teaser-uploader-filelist').prepend(
                                '<div class="ui-widget"><div class="ui-state-highlight ui-corner-all" id="' + file.id + '"><p>' +
                                        file.name + ' (' + plupload.formatSize(file.size) + ')</p><div class="pbar"></div>' +
                                        '</div></div>');
                        $('#' + file.id + " .pbar").progressbar({value: 0});
                });
                up.refresh();
                up.start();
        });

        uploader.bind('UploadProgress', function(up, file) {
                $('#' + file.id + " .pbar").progressbar({value: file.percent});
        });


        uploader.bind('FileUploaded', function(up, file, info) {
                var response = jQuery.parseJSON(info.response);
                if (response.error == false && response.responseText.hasOwnProperty('src')){
                        $('#page-preview-image').attr('src', response.responseText.src);
                }
                else {
                        var errMsg = '';
                        $.each(response.responseText.data, function(k, v){
                                errMsg += '<p>' + v + '</p>';
                        });
                        smoke.alert(errMsg, {'classname':'errors'});
                }
        });
        }
    });

});